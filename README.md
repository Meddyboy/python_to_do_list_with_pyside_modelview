# Tableau d'Application avec PySide2

Ce projet implémente un tableau d'application simple en utilisant PySide2. Il comprend un modèle de tableau, une vue et un délégué personnalisé pour le rendu des cellules.

## Installation

Assurez-vous d'avoir Python installé. Utilisez le gestionnaire de paquets de votre choix pour installer les dépendances.

```bash
pip install PySide2
```

## Utilisation

Exécutez l'application en utilisant le script principal.
```bash
python main.py
```
Le tableau d'application s'affichera avec des cellules contenant des informations fictives. Vous pouvez cliquer sur les cellules pour les éditer.

## Structure du Projet
- main.py: Point d'entrée de l'application.

- cell.py: Définition de la classe Cell pour les données de cellule.

- table_model.py: Implémentation du modèle de tableau.

- delegate.py: Définition du délégué pour le rendu personnalisé des cellules.
 
