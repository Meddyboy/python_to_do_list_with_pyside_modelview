from PySide2 import QtCore

class QModel(QtCore.QAbstractTableModel):
    
    def __init__(self, application, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self.application = application
    
    def rowCount(self, parent=None):
        return self.application.model_info().row_count
    
    def columnCount(self, parent=None):
        return self.application.model_info().column_count

    def data(self, index, role):
        cell = self.application.cell(index.row(),index.column())
        
        if role == QtCore.Qt.DisplayRole:  
            return "{}\nTask to do : {}".format(cell.asset_name, cell.task)
        
        if role == QtCore.Qt.UserRole:  
            return cell
        
    def setData(self ,index ,value ,role):
        self.application.update_cell(index.row(), index.column(),value)
        return False
       
    #def flags(self, index):
    #    return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable
