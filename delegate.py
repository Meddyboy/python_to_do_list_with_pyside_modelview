from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui

COUNT_HEIGHT = 30
PADDING = 20
HEIGHT = 200
MIN_WIDTH = 100

class Delegate(QtWidgets.QStyledItemDelegate):
    
    def __init__(self, parent=None):
        QtWidgets.QStyledItemDelegate.__init__(self, parent)
        self.font = QtGui.QFont()
        self.metrics = QtGui.QFontMetrics(self.font)
        
    def paint(self, painter , option , index):
        cell = index.data(QtCore.Qt.UserRole)
          
        rect_asset_img = QtCore.QRect(
            option.rect.left(),
            option.rect.top(),
            option.rect.width(),
            option.rect.height() - 80
        )
        
        rect_asset_name = QtCore.QRect(
            option.rect.left(),
            option.rect.top() + 75,
            option.rect.width(),
            option.rect.height() 
        )
        
        rect_task = QtCore.QRect(
            option.rect.left(),
            option.rect.top() + 120,
            option.rect.width(),
            30
        )
        
        rect_button = QtCore.QRect(
            option.rect.left(),
            option.rect.top() + option.rect.height() - COUNT_HEIGHT,
            option.rect.width(),
            COUNT_HEIGHT
        )
        

        painter.setBrush(QtCore.Qt.black)
        painter.setPen(QtCore.Qt.black)
        painter.drawImage(
            rect_asset_img,
            QtGui.QImage('./img/logo.jpg')
        )
        painter.drawText(
            rect_asset_name,
            QtCore.Qt.AlignCenter,
            cell.asset_name
        )
        
        painter.setPen(QtCore.Qt.white)
        painter.setBrush(QtCore.Qt.gray)
        painter.drawRect(
            rect_task
        )
        painter.drawText(
            rect_task,
            QtCore.Qt.AlignCenter,
            cell.task
        )
        
        
        '''
        painter.setPen(QtCore.Qt.white)
        painter.setBrush(QtCore.Qt.NoBrush)
        painter.drawText(
            rect_button,
            QtCore.Qt.AlignCenter,
            cell.asset_name
        )
        
        painter.setPen(QtCore.Qt.white)
        painter.drawText(
            rect_count,
            QtCore.Qt.AlignCenter,
            "{}".format(cell.task)
        )
        '''
    
    def sizeHint(self, option, index):
        cell = index.data(QtCore.Qt.UserRole)
        width = max(MIN_WIDTH , self.metrics.width(cell.asset_name) + PADDING *2)
        return QtCore.QSize(width, HEIGHT)