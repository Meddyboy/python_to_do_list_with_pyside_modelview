from cell import Cell

class TableModelInfo:
    
    def __init__(self):
        self.row_count = 0
        self.column_count = 0

class TableModel:
    
    def __init__(self, size=5):
        self._size = size
        self.data = list()
        self.init_cache()
    
    def init_cache(self):
        for row in range(1, self._size + 1):
            if len(self.data) < row:
                self.data.append(list())
            
            for col in range(1, self._size + 1):
                self.data[row - 1].append(Cell(''))
    
    def info(self):
        info = TableModelInfo()
        info.row_count = self._size
        info.column_count = self._size
        return info
     
    def update_cell(self, row, col, value):
        self.data[row][col] = Cell(value)
        return True