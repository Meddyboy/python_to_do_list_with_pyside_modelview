#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
:AUTHOR: meddy boukhedouma
:CONTACT: meddy.boukhedouma@gmail.com
"""

import sys
from application import Application

if __name__ == '__main__':
    app = Application(size=10)
    sys.exit(app.run())