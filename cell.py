class Cell:
    def __init__(self, task):
        self.asset_name = "-NAME ASSET-"
        self._task = 'Current Task'
    
    # a getter function
    @property  
    def task(self):
        return self._task
    
    # a setter function
    @task.setter
    def task(self, task):
        self._task = task
       
    def __repr__(self):
        return "<Cell(Current Task : --> '{}' Name Asset : --> {})>".format(self._task, self.asset_name)