import sys
from PySide2 import QtWidgets
from qmodel import QModel
from view import View
from table_model import TableModel


class Application: 
    def __init__(self, size):
        self.table_model = TableModel(size)
        
        self.app = QtWidgets.QApplication(sys.argv)
        self.qmodel = QModel(self)
        self.view = View(self.qmodel)

    def model_info(self):
        return self.table_model.info()
    
    def cell(self, row, column):
        return self.table_model.data[row][column]
    
    def update_cell(self, row, column, value):
        success = self.table_model.update_cell(row, column, value)     
        if success:
            self.view.cell_updated(row, column)
    
    def run(self):
        self.view.show()
        return self.app.exec_()
