from PySide2 import QtWidgets
from delegate import Delegate


class View(QtWidgets.QTableView):
    
    def __init__(self,model ,parent=None): 
        self.model = model
        
        QtWidgets.QTableView.__init__(self, parent=None)  
        
        self.setModel(self.model)
        self.setItemDelegate(Delegate())
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.resize(1280, 720)
    
    def cell_updated(self, row , column):
        self.resizeRowToContents(row)
        self.resizeColumnToContents(column)